import os
from flask_script import Manager
from app import create_app
from api.services.repo_language import index


app = create_app(os.getenv('BOILERPLATE_ENV') or 'dev')

app.app_context().push()

manager = Manager(app)


@manager.command
def run():
    app.run()


if __name__ == '__main__':
    # index()
    manager.run()
