## Create virtual environnement

```
$ python -m venv env
$ env\Scripts\activate
```

## Installation

Install with pip:

```
$ pip install -r requirements.txt
```
## Run Flask

```
$ python manage.py run
```

## route of api (local)

```
http://127.0.0.1:5000/api/repos
```
