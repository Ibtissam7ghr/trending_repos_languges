from flask import Flask
from flask_restful import Api
from flask import Blueprint
from config import config_by_name
from api.services.repo_language import api

from flask_cors import CORS

# api_bp = Blueprint('api', __name__)
# api = Api(api_bp)


def create_app(config_name):
    app = Flask(__name__)
    CORS(app)
    app.config.from_object(config_by_name[config_name])
   
    app.register_blueprint(api, url_prefix='/api')
    return app

