#!flask/bin/python
from flask import Flask, request, Blueprint
import requests
import itertools 
import collections
from datetime import datetime, timedelta

api = Blueprint('api', __name__)

@api.route('/repos', methods=['GET'])
def index():
    
    date = datetime.today() - timedelta(days=30)
    print(date)
    response = requests.get(url='https://api.github.com/search/repositories',
                            params = {
                                'q': 'created:>{0}'.format(date.strftime("%Y-%m-%d")),
                                'order': 'desc',
                                'sort': 'stars',
                                'page': 1,
                                'per_page': 100
                            })
    
    data = response.json()
    
    key = lambda x : x["language"] if type(x["language"]) == type(str()) else 'null'
    res = {k: format_details(list(v)) for k, v in itertools.groupby(sorted(data['items'], key=key), key=key)}
    
    return res

def format_details(param):
    return {'items': param,'count': len(param)}